"use client";
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { motion } from 'framer-motion';

const Hero = () => {

  return (
    
      <div>
        <div className='text-white max-w-[1240px] py-40 mx-auto px-4  flex md:flex-row flex-col' >
            <div className='bg-[#555555] bg-opacity-25 rounded-md basis-1/2 max-w-[620px] w-full mx-auto text-center flex flex-col justify-center'>
                <motion.img
                    initial={{ opacity: 0, x: -20, scale: 0.9 }}
                    animate={{ opacity: 1, x: 0,  scale: 0.9, transition: { delay: 0.3 } }}
                    exit={{ opacity: 0, x: -20, scale: 0.9}}
                    className="rounded-lg scale-90 shadow-2xl transition-opacity duration-75 bg-[#252529]"
                    src="/ThirdUpload.jpeg"
                    alt="Picture of ThirdUpload"
                />

            </div>


            <div className='basis-1/2 max-w-[620px] w-full mx-auto text-center flex flex-col justify-center'>
                
                
                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.4 } }}
                    exit={{ opacity: 0, x: 20 }}
                >
                    <p className='md:text-6xl sm:text-5xl text-5xl font-bold py-1 md:-py-10'>
                    Welcome to
                    </p>
                </motion.div>
                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.5 } }}
                    exit={{ opacity: 0, x: 20 }}
                >
                    <p className='md:text-5xl sm:text-5xl text-5xl font-bold md:-py-10'>
                    ThirdUpload
                    </p>
                </motion.div>
                

                <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.8 } }}
                    exit={{ opacity: 0, x: 20 }}
                >   
                    <p className='md:text-xl sm:text-lg text-md py-4 md:-py-10'>
                    Your Secure File Storage Provider
                    </p>
                </motion.div>
                
                <motion.div
                  whileHover={{ scale: 1.2 }}
                  whileTap={{
                    scale: 0.8,
                    borderRadius: "100%"
                  }}
                >
                  <div className='relative group'>
                    <div className='absolute inset-0 blur group-hover:bg-[#252529] transition-all duration-300 rounded-lg my-6 mx-auto py-3 w-[200px]'>
                    </div>
                    <Link to="/upload">
                      <button className='relative border-4 border-[#252529] w-[200px] rounded-md font-medium my-6 mx-auto py-3 hover:shadow-lg'>
                        Upload now!
                      </button>
                    </Link>
                  </div>


                </motion.div>


            </div>
          </div>
      </div>
        

  );
};

export default Hero;