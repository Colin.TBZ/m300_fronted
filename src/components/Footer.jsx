import React from 'react';
import { FaLock } from "react-icons/fa";

const Footer = () => {
  return (
    <footer className="footer-bg mt-auto">
      <div className='max-w-[1240px] mx-auto py-16 px-4 grid lg:grid-cols-3 gap-8 text-[white]'>
        <div>
          <h1 className='w-full text-3xl font-bold flex flew-row'><FaLock /> ThirdUpload</h1>
          <div className='flex md:w-[30%]'>
              
          </div>
          <p className='py-4 font-extralight'>Made by Colin Schrepfer | All rights reserved</p>
        </div>
      </div>
    </footer>
    
  );
};

export default Footer;