import React, { useState } from 'react';
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai';
import { Link, NavLink } from 'react-router-dom';
import { FaLock } from "react-icons/fa";

const Navbar = () => {
  const [nav, setNav] = useState(false);

  const handleNav = () => {
    setNav(!nav);
  };

  return (
    <div className='flex justify-between items-center h-24 max-w-[1240px] mx-auto px-4 text-white '>
      <Link to="/">
        <h1 className='w-full text-3xl font-bold text-[white] flex flex-row'><FaLock />ThirdUpload</h1>
      </Link>
      
      <ul className='hidden md:flex'>
        
        <NavLink to="/" activeclassname="active">
        <li className='p-4 text-gray-300 hover:text-white hover:underline underline-offset-2 transition-colors duration-200'>Home</li>
        </NavLink>

        <NavLink to="/files" activeclassname="active">
        <li className='p-4 text-gray-300 hover:text-white hover:underline underline-offset-2 transition-colors duration-200'>Files</li>
        </NavLink>  
        
        <NavLink to="/upload" activeclassname="active">
        <li className='p-4 text-gray-300 hover:text-white hover:underline underline-offset-2 transition-colors duration-200'>Upload</li>
        </NavLink>

        <NavLink to="/subscription" activeclassname="active">
        <li className='p-4 text-gray-300 hover:text-white hover:underline underline-offset-2 transition-colors duration-200'>Subscription</li>
        </NavLink>

      </ul>
      
      <div onClick={handleNav} className='z-20 fixed right-4 block md:hidden '>
          {nav ? <AiOutlineClose size={30}/> : <AiOutlineMenu size={30} />}
      </div>
      <ul className={nav ? 'z-10 fixed right-[-45%] top-0 w-[100%] h-full border-r border-r-gray-900 bg-[#252529]/90 ease-in-out duration-700' : 'fixed ease-in-out duration-800  right-[-100%]'}>
          <h1 className='w-full text-3xl font-bold text-[white] mx-4 my-7 flex flex-row'><FaLock />ThirdUpload</h1>
          <NavLink to="/" activeclassname="active"><li className='p-4 border-b border-gray-600 hover:underline underline-offset-2'>Home</li></NavLink>
          <NavLink to="/files" activeclassname="active"><li className='p-4 border-b border-gray-600 hover:underline underline-offset-2'>Files</li></NavLink>
          <NavLink to="/upload" activeclassname="active"><li className='p-4 border-b border-gray-600 hover:underline underline-offset-2'>Upload</li></NavLink>
          <NavLink to="/subscription" activeclassname="active"><li className='p-4 border-b border-gray-600 hover:underline underline-offset-2'>Subscription</li></NavLink>
          
      </ul>
    </div>
  );
};

export default Navbar;