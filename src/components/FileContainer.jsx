import React, { useState, useEffect } from 'react';
import axios from 'axios';

const FileContainer = () => {
  const [files, setFiles] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('https://172.31.177.161:8604/api/files'); // Assuming an API endpoint for listing files
        setFiles(response.data);
      } catch (error) {
        console.error('Error fetching files:', error);
      }
    };

    fetchData();
  }, []);

  const downloadFile = async (fileName) => {
    try {
      const downloadUrl = `https://172.31.177.161:8604/api/download/${fileName}`; // Construct download URL using filename
      const response = await axios.get(downloadUrl, {
        responseType: 'blob', // Set response type to blob for downloads
      });
      const blob = new Blob([response.data], { type: response.data.type });

      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob); // Create temporary URL for the blob
      link.download = fileName; // Set the download filename
      link.click(); // Simulate a click to trigger download

      // Optionally revoke the temporary object URL after download
      setTimeout(() => URL.revokeObjectURL(link.href), 1000); // Revoke after 1 second
    } catch (error) {
      console.error('Error downloading file:', error);
    }
  };

  return (
    <div className='max-w-[1240px] mx-auto my-10 px-1 gap-8 text-[white] bg-[#555555] bg-opacity-25 rounded-md relative flex flex-col'>
      <h1 className='text-center justify-center md:text-5xl sm:text-4xl text-4xl font-bold py-4 underline'>
        My Files
      </h1>

      <div className='flex flex-wrap p-4 rounded-xl'>
        {/* Render the list of files here */}
        {files.map((fileInfo) => ( 
          <div
            key={fileInfo.id} // Use unique 'id' for key prop
            className='w-full md:w-1/2 lg:w-1/4 px-12 py-20 m-4 rounded-md bg-zinc-600 hover:bg-zinc-700 cursor-pointer'
            onClick={() => downloadFile(fileInfo.filename)} // Pass filename
          >
            <p className='text-white font-medium text-ellipsis overflow-hidden' style={{ maxWidth: '200px' }}>
              {fileInfo.original_name} {/* Display original filename */}
            </p>

          </div>
        ))}
      </div>
    </div>
  );
};

export default FileContainer;
