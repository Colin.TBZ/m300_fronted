"use client";
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { motion } from 'framer-motion';

const Subscription = () => {

  return (
    
      <div>
        <div className='text-white max-w-[1240px] py-40 mx-auto px-8  flex md:flex-col flex-col bg-[#555555] bg-opacity-25 rounded-md relative' >
            <h1 className='text-center justify-center md:text-5xl sm:text-4xl text-4xl font-bold py-4 underline'>
                    Subscriptions
            </h1>
            <div className='max-w-[800px] w-full mx-auto justify-between text-center flex flex-row'>
                
            
                <div className='basis-1/3 max-w-[620px] w-full mx-auto text-center flex flex-col justify-center text-[white] bg-[#252529] rounded-md'>
                    <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}
                    exit={{ opacity: 0, x: 20 }}
                    >
                        <h2 className="text-2xl font-bold mb-4">Free Tier</h2> 
                        {/* Replace this with your pricing details */}
                        <div className="text-3xl font-bold mb-2">Free</div>

                        <ul className="mb-6"> 
                            <li>Basic Encryption</li> 
                            <li>5 GB Storage</li> 
                            <li>Community Support</li>
                        </ul>

                        <button disabled className="border-[#555555] border-4 text-white font-bold py-2 px-4 rounded">
                            Selected
                        </button>
                    </motion.div>
                </div>
                <div className='basis-1/3 max-w-[620px] w-full mx-auto text-center flex flex-col justify-center text-[white] bg-[#252529] rounded-md'>
                    <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.3 } }}
                    exit={{ opacity: 0, x: 20 }}
                    >
                        <h2 className="text-2xl font-bold mb-4">Basic</h2> 
                        {/* Replace this with your pricing details */}
                        <div className="text-3xl font-bold mb-2">$5 per Month</div>

                        <ul className="mb-6"> 
                            <li>Premium File Encryption</li> 
                            <li>20 GB Storage</li> 
                            <li>Basic Support</li>
                        </ul>

                        <button className="bg-[#555555] hover:bg-[#404040] text-white font-bold py-2 px-4 rounded">
                            Subscribe
                        </button>
                    </motion.div>
                </div>
                <div className='basis-1/3 max-w-[620px] w-full mx-auto text-center flex flex-col justify-center text-[white] bg-[#252529] rounded-md'>
                    <motion.div
                    initial={{ opacity: 0, x: 20 }}
                    animate={{ opacity: 1, x: 0, transition: { delay: 0.4 } }}
                    exit={{ opacity: 0, x: 20 }}
                    >
                        <h2 className="text-2xl font-bold mb-4">Premium</h2> 
                        {/* Replace this with your pricing details */}
                        <div className="text-3xl font-bold mb-2">$20 per Month</div>

                        <ul className="mb-6"> 
                            <li>Premium File Encryption</li> 
                            <li>50 GB Storage</li> 
                            <li>Priority Support</li>
                        </ul>

                        <button className="bg-[#555555] hover:bg-[#404040] text-white font-bold py-2 px-4 rounded">
                            Subscribe
                        </button>
                    </motion.div>
                </div>
            </div>
          </div>
      </div>
        

  );
};

export default Subscription;