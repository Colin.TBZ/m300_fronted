import React, { useState } from 'react';
import { MdDriveFolderUpload } from 'react-icons/md';
import axios from 'axios';

const FileUpload = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadStatus, setUploadStatus] = useState(null); // State to track upload status

  const handleChange = (event) => {
    setSelectedFile(event.target.files[0]);
    setUploadStatus(null); // Clear any previous status message
  };

  const handleUpload = async () => {
    if (!selectedFile) return;

    setUploadStatus('Uploading...'); // Set initial uploading status

    try {
      const formData = new FormData();
      formData.append('file', selectedFile);

      const response = await axios.post('https://172.31.177.161:8604/api/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      console.log('File uploaded successfully:', response.data);
      setUploadStatus('File uploaded successfully!'); // Set success message
      setSelectedFile(null); // Clear file selection
      // Handle successful upload (e.g., trigger UI update, fetch updated file list)
    } catch (error) {
      console.error('Upload failed:', error);
      setUploadStatus('Upload failed. Please try again.'); // Set error message
      // Handle upload error (e.g., display error message)
    }
  };

  return (
    <div className='max-w-[1240px] mx-auto my-10 px-1 gap-8 text-[white] bg-[#555555] bg-opacity-25 rounded-md relative flex flex-col'>
      <div className='max-w-[800px] w-full mx-auto text-center flex flex-col justify-center'>
        <h1 className='text-center justify-center md:text-5xl sm:text-4xl text-4xl font-bold py-4 flex flex-row underline'>
          <MdDriveFolderUpload size={50} /> File Upload <MdDriveFolderUpload size={50} />
        </h1>
        <div className="file-upload flex flex-col items-center p-11">
          <h6 className="text-xl mb-4">Upload a file or Drop it in</h6>
          <input
            type="file"
            onChange={handleChange}
            className="px-24 py-16 rounded-md border border-gray-300 focus:outline-none focus:ring-white"
          />
          <button
            onClick={handleUpload}
            className="mt-2 px-4 py-2 bg-[#252529] text-white rounded-md focus:outline-none focus:ring-white hover:bg-[#36363b]"
          >
            Upload
          </button>
          {/* Display upload status message */}
          {uploadStatus && (
            <p className={`mt-2 text-white ${uploadStatus === 'Uploading...' ? 'text-yellow-400' : uploadStatus === 'File uploaded successfully!' ? 'text-green-500' : 'text-red-500'}`}>
              {uploadStatus}
            </p>
          )}
          {selectedFile && (
            <p className="mt-2 text-white">Selected: {selectedFile.name}</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default FileUpload;
