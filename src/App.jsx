import { useState } from 'react'
import { BrowserRouter as Link, Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import FileUpload from './components/FileUpload';
import Hero from './components/Hero';
import FileContainer from './components/FileContainer';
import Subscription from './components/Subscription';


function App() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <Navbar />
      <Routes>
          <Route path="/" element={<Hero />} />
          <Route path="/subscription" element={<Subscription />} />
          <Route path="/upload" element={<FileUpload />} />
          <Route path="/files" element={<FileContainer />} />
      </Routes>
      <Footer />
      
    </div>
  )
}

export default App